# afas_api  

This project is a on-going coding exercise for me to learn Python.   
For now I only coded the GET REST API script, but I am planning to do the other options as well.

## afas_get  

This is a funcion to call the AFAS Profit GET REST API.  
I assume you have the required knowledge of AFAS before diving into this.

So, what you need is a connection token, setup in the app connector in AFAS Profit.   
You also need to add a GetConnector definition.

URL als operatortypes can be found here: https://help.afas.nl/help/NL/SE/App_Cnr_Rest_Api.htm

### Preparation for the call  

The config list is manditory.  
``` config  = [ app token , https://12345.rest.afas.online/profitrestservice/] ```

There are two lists to influence the outcome of the API call  
``` filters = [{'filter_id': '', 'filter_value': '', 'operator_type': '1'}, {'filter_id': '', 'filter_value': '', 'operator_type': '1'}] ```  
``` orderby = [{'order_id': '', 'asc_desc': 'ASC'}, {'order_id': '', 'asc_desc': 'DESC'}] ```

### The actual call  

``` afas_get(config = config, connector_id = ' ... connectorname ... ', filters = filters, filter_operator = 'AND/OR', order_by = orderby, skip = '', take = '') ```
import base64
import requests
import sys

## GET call to the AFAS API.
## The config argument should consist of a list with [API token, AFAS endpoint rest call]
## connector_name is the id as specified in AFAS Profit (https://help.afas.nl/help/NL/SE/App_Cnr_Rest_Api.htm)
## The filters param is a list with nested dictionary, which contains the fieldname, filtervalue, operator_type, and operator.
## All operator_type are found on the same help page as stated before.

def afas_get(config, connector_id, filters = None, filter_operator = 'AND', order_by = None, skip = '-1', take = '-1'):
    
    # Building the required AFAS API header
    base64String = base64.b64encode(bytes(config[0], 'utf-8'))
    authValue = 'AfasToken ' + base64String.decode('utf-8')
    header = {'Authorization': authValue}

    # Building the payload to the GET request
    param = {'skip': skip, 'take': take}

    # Building the filter
    if filters:
        filter_fieldids = ''
        filter_values = ''
        operator_type = ''

        if filter_operator == 'AND':
            filter_operator = ','
        else:
            filter_operator = ';'

        for filter_definition in filters:
            filter_fieldids += filter_definition['filter_id'] + filter_operator
            filter_values += filter_definition['filter_value'] + filter_operator
            operator_type += filter_definition['operator_type'] + filter_operator
    
        param.update({'filterfieldids': filter_fieldids, 'filtervalues': filter_values, 'operatortypes': operator_type})

    # Building the sort as specified by the API
    if order_by:
        orderbyfieldids = ''

        for index, order_by_definition in enumerate(order_by, 0):
            if order_by_definition['asc_desc'] == 'DESC':
                asc_desc_type = '-'
            else:
                asc_desc_type = ''

            if index > 0:
                orderbyfieldids += ',' + asc_desc_type + order_by_definition['order_id']
            else:
                orderbyfieldids += asc_desc_type + order_by_definition['order_id']

        param.update({'orderbyfieldids': orderbyfieldids})

    # The actual GET call to AFAS.
    try:
        response = requests.get(config[1] +'connectors/'+ connector_id, params=param, headers=header)
    except:
        print("System error:", sys.exc_info()[1])
    else:  
        if response.status_code != 200:
            print(f'Error code    >> {response.status_code}')
            print(f'Error message >> {response.reason}')
            return -1
        else:
            return response.json()['rows']

